package model_Entities;

import java.util.List;

public class Course {
	private String course_id;
	private String title;
	private Department Department;
	private int credits;
	private List<Section> Section;
	private List<Prereq> Prereq;
	private List<Takes> Takes;
	private List<Teaches> Teaches;
}
